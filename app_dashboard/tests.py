from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class DashboardUnitTest(TestCase):

    def test_hello_name_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)